#!/bin/bash
project_name=ahp-playbook

# adding user to sudoers
sudo su -c "echo '$USER ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers"

# installing curl, git, ansible and python
sudo apt-get update && sudo apt-get install curl git ansible python-apt -y

# enabling git lfs
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs -y
git-lfs install

# getting git user and email
read -p "git author: " GIT_AUTHOR
read -p "git email: "  GIT_EMAIL

git config --global user.name "$(echo $GIT_AUTHOR)"
git config --global user.email "$(echo $GIT_EMAIL)"

# cloning ahp-playbook project
git clone git@gitlab-b2b.atech.com.br:ahead/${project_name}.git

# starting ansible playbook conf.
cd ${project_name} && bash setup.sh
